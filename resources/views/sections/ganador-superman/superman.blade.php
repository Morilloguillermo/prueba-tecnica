<div class="container mt-5">
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
			@include('sections.formulario-ganador.formulario-ganador')
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<img src="{{asset('/img/superman.png')}}" alt="" class="img-fluid mx-auto d-block">
		</div>
	</div>
</div>