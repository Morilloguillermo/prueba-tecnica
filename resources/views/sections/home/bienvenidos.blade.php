<div class="container bienvenido-cookie1-bg">
	<div class="bienvenido-cookie3-bg">
		<div class="bienvenido-cookie2-bg">
			<h1 class="text-center new-comic-font text-uppercase text-white bienvenidos-text animate__repeat-3 animate__animated animate__pulse">Bienvenido</h1>
			<p class="text-white p-1 text-center">Por favor, introduce tu fecha de nacimiento <br> para continuar navegando:</p>
		</div>
		<form id="age-verify">
			<div class="d-flex flex-row justify-content-around  mb-3">
			    <div class="p-2 bd-highlight form-group animate__heartBeat animate__animated animate__repeat-3">
				    <select class="form-control" id="day">
				      	<option value="1">1</option>
				      	<option value="2">2</option>
				      	<option value="3">3</option>
				      	<option value="4">4</option>
				      	<option value="5">5</option>
				    </select>
			    </div>
			    <div class="p-2 bd-highlight form-group animate__heartBeat animate__animated animate__repeat-3">
				    <select class="form-control" id="month">
				      	<option value="1">1</option>
				      	<option value="2">2</option>
				      	<option value="3">3</option>
				      	<option value="4">4</option>
				      	<option value="5">5</option>
				    </select>
			    </div>
			    <div class="p-2 bd-highlight form-group animate__heartBeat animate__animated animate__repeat-3">
				    <select class="form-control" id="year">
				      	<option value="2019">2019</option>
				      	<option value="2000">2000</option>
				      	<option value="2020">2020</option>
				      	<option value="2002">2002</option>
				      	<option value="2003">2003</option>
				    </select>
			    </div>
		 	</div>
		 	<div class="container">
				<div class="d-flex flex-row justify-content-center bd-highlight mb-3 pt-5">
				    <div class="p-3  bd-highlight">
				    	<button type="submit" class="btn btn-light mb-2 px-4 button-shadow">Entrar&nbsp;&nbsp;&nbsp;<i class="fas fa-chevron-right"></i></button>
				    </div>
			 	</div>
			</div>

		</form>
	</div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center main-color py-5">
        <h1 class="new-comic-font title-text">¡Oops!</h1>
        <p class="p-1">No cumples con la edad requerida para  <br>participar en la promoción <br><strong>¡Inténtalo de nuevo!</strong></p>
      </div>
    </div>
  </div>
</div>