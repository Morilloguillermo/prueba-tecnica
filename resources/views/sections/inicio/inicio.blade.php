<div class="container py-3">
	<div class="row h-100" style="">
        <div class="col-xl-6 col-lg-12 col-sm-12 col-xs-12 align-self-center no-padding">
            <div>
                	<h1 data-wow-delay="0.1s" class="wow fadeInLeft  text-center text-white title-text new-comic-font animate__animated animate__bounce">
                	Un SuperHéroe Funko <br>de la Liga de la Justicia <br> puede ser tuyo!
                </h1>
                <p class="text-white text-center py-4 animate__animated animate__bounce">
                	Encuentra el código en tus Mini ChipsAhoy!, Mini Oreo,<br> Mini Príncipe, Príncipe Estrellas, Principe A Cucharadas,<br> Principe Figuritas y Osito Lulu
                </p>
				<div class="d-flex flex-row justify-content-center bd-highlight mb-3 animate__animated animate__heartBeat animate__repeat-3">
				    <div class="p-3  bd-highlight">
				    	<a href="/codigo" class="btn btn-light mb-2 px-4 button-shadow">Consigue el tuyo  <i class="fas fa-chevron-right"></i></a>
				    </div>
			 	</div>
            </div>
        </div>
        <div data-wow-delay="0.1s" class=" animate__animated animate__fadeIn col-xl-6 col-lg-12 col-sm-12 col-xs-12 align-self-center no-padding d-none d-sm-none d-lg-none d-xl-block">
            <a href="#productos-asociados">
                <img src="{{asset('/img/inicio-image.png')}}" alt="" class="img-fluid mx-auto d-block center-block">
            </a>
        </div>
    </div>
</div>

<div class="container py-4 animate__animated animate__shakeX animate__repeat-3">
	<p class="text-center text-white">Descubre toda la gama y dónde encontrar el código</p>
	<section id="section06" class="demo">
	  	<a href="#productos"><span></span></a>
	</section>
</div>

@include('sections.productos.productos')