<div class="container mt-5">
	<h1 class="new-comic-font title-text text-center text-white">¡Un verdadero SuperHéroe <br> no deja de intentarlo!</h1>
	<p class="mt-4 text-center text-white">Lo sentimos, este código no tiene premio. Pero aún estás a tiempo de seguir participando <br> y conseguir tu superhéroues Funko. ¡Vamos!</p>
	<div class="d-flex flex-row justify-content-center bd-highlight mb-3">
	    <div class="p-3  bd-highlight">
	    	<a href="/codigo" class="btn btn-light mb-2 px-4 button-shadow">Introducir otro código&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-chevron-right"></i></a>
	    </div>
 	</div>
 	<div class="col-12 p-0 ">
 		<img src="{{asset('/img/bg-productos.png')}}" alt="" class="img-fluid mx-auto d-block ">
 	</div>
</div>