<form action="{{route('enviarDatos')}}" method="POST" autocomplete="off" name="formulario_contacto" id="formulario_contacto" >
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<div class="form__group_generic animate__fadeInLeft animate__animated field">
		<input type="input" class="form__field_generic" placeholder="Nombre" name="name" id='name' required />
		<label for="name" class="form__label_generic">Nombre</label>
	</div>
	<div class="form__group_generic animate__fadeInLeft animate__animated field">
		<input type="input" class="form__field_generic" placeholder="Apellidos" name="fullname" id='fullname' required />
		<label for="fullname" class="form__label_generic">Apellidos</label>
	</div>
	<div class="row">
		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<div class="form__group_generic animate__fadeInLeft animate__animated field p-0">
				<input type="input" class="form__field_generic" placeholder="Dirección" name="address" id='address' required />
				<label for="address" class="form__label_generic">Dirección</label>
			</div>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="form__group_generic animate__fadeInLeft animate__animated field p-0">
				<input type="input" class="form__field_generic" placeholder="Código Postal" name="cp" id='cp' required />
				<label for="cp" class="form__label_generic">Código Postal</label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="form__group_generic animate__fadeInLeft animate__animated field p-0">
				<input type="input" class="form__field_generic" placeholder="Provincia" name="province" id='province' required />
				<label for="province" class="form__label_generic">Provincia</label>
			</div>
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="form__group_generic animate__fadeInLeft animate__animated field p-0">
				<input type="input" class="form__field_generic" placeholder="Localidad" name="local" id='local' required />
				<label for="local" class="form__label_generic">Localidad</label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="form__group_generic animate__fadeInLeft animate__animated field p-0">
				<input type="input" class="form__field_generic" placeholder="Teléfono" name="phone" id='phone' required />
				<label for="phone" class="form__label_generic">Teléfono</label>
			</div>
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="form__group_generic animate__fadeInLeft animate__animated field p-0">
				<input type="date" class="form__field_generic" placeholder="Fecha de nacimiento" name="bd" id='bd' required />
				<label for="bd" class="text-white">Fecha de nacimiento</label>
			</div>
		</div>
	</div>
	<div class="d-flex flex-row justify-content-center bd-highlight my-3">
	    <div class="p-3  bd-highlight">
	    	<button type="submit" class="btn btn-light btn-lg mb-2 px-4 button-shadow">
				Enviar&nbsp;&nbsp;&nbsp;&nbsp; <i class="fas fa-chevron-right"></i>
			</button>
	    </div>
 	</div>
</form>