<div class="bg-productos">
	<div class="container mt-5" id="productos">
		<div class="d-flex align-items-center pl-5 ">
			<div class="p-2"><br><br><br><br><br><br><br><br>
				<p class="text-center text-white">Localiza el código que se encuentra <br> impreso en el interior del pack</p>
				<a href="#">
					<div  class="p-3 bg-codigo animate__fadeInTopLeft animate__animated">
						<div class="w-100 bg-dark p-4"></div>
						<p class="new-comic-font lineheigth pt-3 codigo-text" style="color: #FFFF00; text-shadow: 3px 3px #003BA8;">Encuentra tu código  <i class="fas fa-reply fa-w-16 fa-spin fa-lg"></i><br> en el interior del pack </p>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="container py-4 animate__animated animate__shakeX animate__repeat-3">
	<p class="text-center text-white"><a href="/codigo" class="text-white">¡Participa con tu código y gana tu SuperHéroe Funko!</a></p>
	<section id="section06" class="demo">
	  	<a href="#productos"><span></span></a>
	</section>
</div>