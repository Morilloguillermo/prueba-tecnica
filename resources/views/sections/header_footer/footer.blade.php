<div class="container-fluid">
	<div class="d-flex flex-row justify-content-around  mb-3 pt-5">
	    <div class="p-2 bd-highlight"><img src="{{asset('img/footer-1.png')}}" alt="" class="img-fluid mx-auto d-block "></div>
	    <div class="p-2 bd-highlight"><img src="{{asset('img/footer-2.png')}}" alt="" class="img-fluid mx-auto d-block "></div>
	    <div class="p-2 bd-highlight"><img src="{{asset('img/footer-3.png')}}" alt="" class="img-fluid mx-auto d-block "></div>
 	</div>
 	<div class="container text-center py-2">
 		<ul class="list-inline" id="footer_menu">
		   	<li class="list-inline-item">
		   		<a class="text-white" target="_blank" href="#">Política de cookies</a>
		   	</li>
		   	<li class="list-inline-item">
		   		<a class="text-white" target="_blank" href="#">Aviso de Privacidad</a>
		   	</li>
		   	<li class="list-inline-item">
		   		<a class="text-white" target="_blank" href="#">Términos de uso</a>
		   	</li>
		   	<li class="list-inline-item">
		   		<a class="text-white" target="_blank" href="#">Contacto</a>
		   	</li>
		   	<li class="list-inline-item">
		   		<a class="text-white prueba" target="_blank" href="#">Datos de la compañia</a>
		   	</li>
		</ul>
 	</div>
</div>