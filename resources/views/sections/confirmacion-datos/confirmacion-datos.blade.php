<div class="container py-3">
	<div class="row h-100" style="">
        <div class="col-xl-6 col-lg-12 col-sm-12 col-xs-12 align-self-center no-padding">
            <div>
                	<h1 data-wow-delay="0.1s" class=" wow animate__animated animate__bounce  text-center text-white title-text new-comic-font">
                	¡Perfecto!
                </h1>
                <p class="text-white text-center py-4 wow animate__animated animate__bounce">
                	Nos pondremos en contacto contigo en breve <br> para darte más detalles sobre tu premio
                </p>
				<div class="d-flex flex-row justify-content-center bd-highlight mb-3 wow animate__animated animate__bounce">
				    <div class="p-3  bd-highlight">
				    	<a href="/codigo" class="btn btn-light mb-2 px-4 button-shadow">Introducir otro código  <i class="fas fa-chevron-right"></i></a>
				    </div>
			 	</div>
            </div>
        </div>
        <div data-wow-delay="0.5s" class=" wow animate__animated animate__bounce col-xl-6 col-lg-12 col-sm-12 col-xs-12 align-self-center no-padding d-none d-sm-none d-lg-none d-xl-block">
            <a href="#productos-asociados">
                <img src="{{asset('/img/inicio-image.png')}}" alt="" class="img-fluid mx-auto d-block center-block ">
            </a>
        </div>
    </div>
</div>