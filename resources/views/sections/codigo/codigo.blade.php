<div class="container mt-5">
	<form role="form" name="catalogo_form" id="catalogo_form" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<div class="row">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<h1 class="text-white new-comic-font">Comprueba tu código</h1>
			  	<input type="input" class="form__field text-uppercase animate__animated animate__heartBeat animate__repeat-3" placeholder="00000000" name="promotionalCode" id='promotionalCode' required  maxlength="8" />
			  	<div class="form__group_generic field">
  					<input type="input" class="form__field_generic" placeholder="Escribe aquí tu email" name="email" id='email' required />
  					<label for="email" class="form__label_generic">Escribe aquí tu email</label>
  				</div>
				<div class="mt-4 text-white lineheigth">
					<label class="contenedor animate__fadeInLeft animate__animated">
						<small>Declaro ser mayor de 18 años y acepto las <a href="#" data-toggle="modal" data-target="#basesLegales">bases legales</a> de esta promoción</small>
					  	<input type="checkbox" required="required" name="bases_legales" id="bases_legales">
					  	<span class="checkmark"></span>
					</label>
					<label class="contenedor animate__fadeInLeft animate__animated">
						<small>Acepto los <a href="#" data-toggle="modal" data-target="#terminosyCondiciones">terminos y condiciones</a> de esta promoción</small>
					  	<input type="checkbox" required="required" name="terminos_condiciones" id="terminos_condiciones">
					  	<span class="checkmark"></span>
					</label>
					<label class="contenedor animate__fadeInLeft animate__animated">
						<small>Deseo recibir información sobre promociones futuras de Mondelez</small>
					  	<input type="checkbox" name="alta_newsletter" id="alta_newsletter">
					  	<span class="checkmark"></span>
					</label>
				</div>
				<div class="animate__fadeInLeft animate__animated">
          <small class="text-white ">Valoramos su confianza al compartir sus datos personales con nosotros. Siempre tratamos sus datos de una manera justa y respetuosa limitada al propósito mencionado anteriormente. Si desean manejar mas sobre como manejar sus datos, lea nuestro <a href="#" data-toggle="modal" data-target="#avisodePrivacidad">Aviso de privacidad</a></small>
        </div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<img src="/img/formcode-image.png" alt="" class="img-fluid mx-auto d-block">
			</div>
		</div>
		<div class="d-flex flex-row justify-content-center bd-highlight my-3">
		    <div class="p-3  bd-highlight">
		    	<button
					id="boton-enviar"
					class="g-recaptcha btn btn-light btn-lg mb-2 px-4 button-shadow"
					data-sitekey="6LdrNWEUAAAAADupAdyLrYqsLsFRVJiBKMivzso7"
					data-callback="enviar_catalogo"
				>
					Comprobar <i class="fas fa-chevron-right"></i>
				</button>
		    </div>
	 	</div>
	</form>
</div>


<!-- Modal -->
<div class="modal fade" id="basesLegales" tabindex="-1" role="dialog" aria-labelledby="basesLegalesLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="basesLegalesLabel">Bases Legales</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem ducimus saepe sequi qui molestias recusandae accusantium sed, dolore suscipit voluptatem fugit harum soluta nemo nostrum iure ut amet eos blanditiis!</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis repellendus nihil sint at corporis sapiente quia, dolorem dolores quis necessitatibus architecto assumenda tempore eius unde nisi sequi eos error sit. Qui, quas rem animi odio ad aut sit laudantium harum quibusdam cum facere incidunt illum ratione hic vel fugiat itaque.</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi expedita numquam amet, iure ex at excepturi dignissimos debitis, cumque eum provident aperiam adipisci pariatur iste quae molestias deleniti id sint quibusdam, consequatur qui! Iusto magni officia cum voluptatum libero! Error, velit, ducimus? Nostrum iure optio beatae minus, quisquam! Ipsam unde aperiam autem consectetur omnis provident. Tenetur vero officiis libero optio, sint eaque dolor incidunt fugiat velit hic ut exercitationem voluptate eum, nihil quis ea quaerat. Eaque expedita ut hic voluptate.</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi numquam et iusto vero! Nihil, quo, ut. Iure minus aliquid alias quis, quas magni, eum praesentium porro libero deserunt voluptatum facere perferendis unde, voluptates obcaecati facilis excepturi eveniet quisquam accusamus dolorem earum. Numquam iusto est officia assumenda sint itaque nemo voluptatibus optio voluptas a non, maxime, ratione corporis maiores totam illum quaerat, perspiciatis nesciunt? Dolore odio, illo magnam ipsam repudiandae doloribus maxime ut eveniet atque, ea quos eligendi omnis asperiores fugit soluta a sit optio deserunt tenetur ducimus enim quam rerum fuga! Facere omnis provident libero! Labore suscipit aut possimus veritatis quam, ducimus facilis autem, nisi itaque ad nihil, reprehenderit adipisci libero quae! Suscipit quidem consectetur, officiis perspiciatis quis aut amet!</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, molestiae.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="terminosyCondiciones" tabindex="-1" role="dialog" aria-labelledby="terminosyCondicionesLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="terminosyCondicionesLabel">Terminos y condiciones</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem ducimus saepe sequi qui molestias recusandae accusantium sed, dolore suscipit voluptatem fugit harum soluta nemo nostrum iure ut amet eos blanditiis!</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis repellendus nihil sint at corporis sapiente quia, dolorem dolores quis necessitatibus architecto assumenda tempore eius unde nisi sequi eos error sit. Qui, quas rem animi odio ad aut sit laudantium harum quibusdam cum facere incidunt illum ratione hic vel fugiat itaque.</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi expedita numquam amet, iure ex at excepturi dignissimos debitis, cumque eum provident aperiam adipisci pariatur iste quae molestias deleniti id sint quibusdam, consequatur qui! Iusto magni officia cum voluptatum libero! Error, velit, ducimus? Nostrum iure optio beatae minus, quisquam! Ipsam unde aperiam autem consectetur omnis provident. Tenetur vero officiis libero optio, sint eaque dolor incidunt fugiat velit hic ut exercitationem voluptate eum, nihil quis ea quaerat. Eaque expedita ut hic voluptate.</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi numquam et iusto vero! Nihil, quo, ut. Iure minus aliquid alias quis, quas magni, eum praesentium porro libero deserunt voluptatum facere perferendis unde, voluptates obcaecati facilis excepturi eveniet quisquam accusamus dolorem earum. Numquam iusto est officia assumenda sint itaque nemo voluptatibus optio voluptas a non, maxime, ratione corporis maiores totam illum quaerat, perspiciatis nesciunt? Dolore odio, illo magnam ipsam repudiandae doloribus maxime ut eveniet atque, ea quos eligendi omnis asperiores fugit soluta a sit optio deserunt tenetur ducimus enim quam rerum fuga! Facere omnis provident libero! Labore suscipit aut possimus veritatis quam, ducimus facilis autem, nisi itaque ad nihil, reprehenderit adipisci libero quae! Suscipit quidem consectetur, officiis perspiciatis quis aut amet!</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, molestiae.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="avisodePrivacidad" tabindex="-1" role="dialog" aria-labelledby="avisodePrivacidadLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="avisodePrivacidadLabel">Aviso de privacidad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem ducimus saepe sequi qui molestias recusandae accusantium sed, dolore suscipit voluptatem fugit harum soluta nemo nostrum iure ut amet eos blanditiis!</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis repellendus nihil sint at corporis sapiente quia, dolorem dolores quis necessitatibus architecto assumenda tempore eius unde nisi sequi eos error sit. Qui, quas rem animi odio ad aut sit laudantium harum quibusdam cum facere incidunt illum ratione hic vel fugiat itaque.</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi expedita numquam amet, iure ex at excepturi dignissimos debitis, cumque eum provident aperiam adipisci pariatur iste quae molestias deleniti id sint quibusdam, consequatur qui! Iusto magni officia cum voluptatum libero! Error, velit, ducimus? Nostrum iure optio beatae minus, quisquam! Ipsam unde aperiam autem consectetur omnis provident. Tenetur vero officiis libero optio, sint eaque dolor incidunt fugiat velit hic ut exercitationem voluptate eum, nihil quis ea quaerat. Eaque expedita ut hic voluptate.</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi numquam et iusto vero! Nihil, quo, ut. Iure minus aliquid alias quis, quas magni, eum praesentium porro libero deserunt voluptatum facere perferendis unde, voluptates obcaecati facilis excepturi eveniet quisquam accusamus dolorem earum. Numquam iusto est officia assumenda sint itaque nemo voluptatibus optio voluptas a non, maxime, ratione corporis maiores totam illum quaerat, perspiciatis nesciunt? Dolore odio, illo magnam ipsam repudiandae doloribus maxime ut eveniet atque, ea quos eligendi omnis asperiores fugit soluta a sit optio deserunt tenetur ducimus enim quam rerum fuga! Facere omnis provident libero! Labore suscipit aut possimus veritatis quam, ducimus facilis autem, nisi itaque ad nihil, reprehenderit adipisci libero quae! Suscipit quidem consectetur, officiis perspiciatis quis aut amet!</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, molestiae.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<button type="button" id="usedCodeErrorAlert" class="btn btn-primary d-none" data-toggle="modal" data-target="#usedCodeError"></button>
<div class="modal fade" id="usedCodeError" tabindex="-1" role="dialog" aria-labelledby="usedCodeErrorLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center main-color py-5">
        <h1 class="new-comic-font title-text">¡Oops!</h1>
        <p class="p-1">El código promocional que has ingresado ya <br>ha sido usado.<br><strong><br>¡Inténtalo de nuevo!</strong></p>
      </div>
    </div>
  </div>
</div>