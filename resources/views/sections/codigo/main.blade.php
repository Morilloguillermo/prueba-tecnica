@extends('layout.app')
@section('content')
	@include('sections.codigo.codigo')
@endsection

@section('stylesheet')
<link rel="stylesheet" href="{{ asset('css/toastr.css') }}">
@endsection

@section('scripts')
<script src="//code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="{{ asset('js/toastr.js')}}"></script>
@endsection
