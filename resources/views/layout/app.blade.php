<!DOCTYPE html>
<html lang="es-ES">
    <head>
	<meta charset="utf-8">
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon">        
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="name" content="content">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{csrf_token()}}">
    @yield('title')
    @yield('metas')
        
    </head>

    <body>
        
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
        @yield('stylesheet')
        <main id="main" >
        	<div class="noisy">
	        	<div class="lines-comic">
	        		@include('sections.header_footer.header')
		            @yield('content')
		            @include('sections.header_footer.footer')
		        </div>
	        </div>
        </main>
        
        
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://momentjs.com/downloads/moment.min.js"></script>
        <script src="https://kit.fontawesome.com/6e54860a37.js" crossorigin="anonymous"></script>
        

        <script src="{{ asset('js/app.js')}}" type="text/javascript" charset="utf-8" ></script>
        @yield('scripts')
        <script src="{{ asset('js/formulario.js')}}"></script>
        <script src="{{ asset('js/wow.min.js')}}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        
    </body>
</html>
