<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Promotionalcode;
use Faker\Generator as Faker;

$factory->define(Promotionalcode::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->randomNumber($nbDigits = 8),
        'is_used' => 0,
        'premio_type' => numberBetween($min = 1, $max = 3)
    ];
});



