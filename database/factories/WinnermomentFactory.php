<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Winnermoment;
use Faker\Generator as Faker;

$factory->define(Winnermoment::class, function (Faker $faker) {
    return [
        'is_blocked' => 0,
        'promotionalcode_id' => 0,
        'date_hour' => $faker->dateTime()
    ];
});
