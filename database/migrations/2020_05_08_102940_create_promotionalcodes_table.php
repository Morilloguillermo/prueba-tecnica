<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionalcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotionalcodes', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->boolean('is_used')->default(0);
            $table->integer('premio_type')->nullable();
            $table->integer('winnermoment_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotionalcodes');
    }
}
