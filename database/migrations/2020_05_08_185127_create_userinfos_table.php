<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserinfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userinfos', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('fullname')->nullable();
            $table->string('address')->nullable();
            $table->string('cp')->nullable();
            $table->string('province')->nullable();
            $table->string('local')->nullable();
            $table->string('phone')->nullable();
            $table->string('bd')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userinfos');
    }
}









