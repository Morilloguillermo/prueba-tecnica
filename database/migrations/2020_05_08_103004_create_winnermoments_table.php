<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWinnermomentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('winnermoments', function (Blueprint $table) {
            $table->id();
            $table->boolean('is_blocked')->default(0);
            $table->integer('promotionalcode_id')->unsigned()->nullable();
            $table->dateTime('user_date')->nullable();
            $table->dateTime('date_hour')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('winnermoments');
    }
}
