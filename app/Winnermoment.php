<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Promotionalcode;

class Winnermoment extends Model
{
    protected $fillable = [
        'id', 'promotionalcode_id', 'date_hour', 'is_blocked', 'user_date'
    ];

    public function promotionalcode()
    {
        return $this -> hasOne(Promotionalcode::class);
    }
}
