<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Winnermoment;

class Promotionalcode extends Model
{
    protected $fillable = [
        'code', 'is_used', 'winnermoment_id', 'premio_type'
    ];

    public function winnermoment()
    {
        return $this -> hasOne(Winnermoment::class);
    }
}





