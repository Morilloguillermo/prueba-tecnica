<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userinfo extends Model
{
    protected $fillable = [
        'name',
		'fullname',
		'address',
		'cp',
		'province',
		'local',
		'phone',
		'bd'
    ];
}


