<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App;
use App\Promotionalcode;
use App\Winnermoment;
use App\Userinfo;
use Carbon\Carbon;

class ValidatecodeController extends Controller
{
    public function enviarCatalogo(Request $request) {
                
        $nombre = $request['codigo_promocional'];
        $telefono = $request['email'];
        $bl = $request['bases_legales'];
        $tc = $request['terminos_condiciones'];
        $email = $request['alta_newsletter'];
        $recaptchaURL = 'https://www.google.com/recaptcha/api/siteverify';
        $vars = 'secret=6LdrNWEUAAAAAMRWjvt6X3aFO_cU2fTLbnYMbSJW&response='. $request['g-recaptcha-response'];
        $ch = curl_init( $recaptchaURL );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $vars);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response_captcha = json_decode(curl_exec( $ch ), true);

        if ($response_captcha) {
            $response = [
                'resultado' => $resultado,
            ];
            return json_encode($response);
        } else {
            return false;
        }
    }

    public function verificarCodigo(Request $request, $codigo){

        $response =  Promotionalcode::where('code', $codigo)->orWhere('is_used', 0)->first();

        $used_code = Promotionalcode::select('is_used')->where('code', $codigo)->first();

        //  Si existe el codigo promocional
        if($response){
            if($used_code){
                //  Si codigo promocional no ha sido canjeado
                if($used_code->is_used == false){
                    $updateDataPromotionalCode =  Promotionalcode::select('id')->where('code', $codigo)->first();
                    $updateDataPromotionalCode->is_used = '1';
                    $updateDataPromotionalCode->save();

                    $mytime = now();
                    $verifyDateWinner = Winnermoment::select('date_hour')->where('is_blocked', 0)->first();

                    if($mytime >= $verifyDateWinner->date_hour){
                        $getPromotionalCodeId = Promotionalcode::select('id')->where('code', $codigo)->first();
                        $promoCodeId = $getPromotionalCodeId->id;

                        $updateDateWinner = Winnermoment::where('date_hour', $verifyDateWinner->date_hour)->orWhere('is_blocked', 0)->first();
                        $updateDateWinner->is_blocked = '1';
                        $updateDateWinner->promotionalcode_id = $promoCodeId;
                        $updateDateWinner->user_date = $mytime;
                        $updateDateWinner->save();

                        $updatePromotionalCode = Promotionalcode::where('id', $promoCodeId)->first();
                        $updatePromotionalCode->winnermoment_id = $updateDateWinner->id;
                        $updatePromotionalCode->save();

                        //$mensaje = 'codigo correcto, has ganado';

                        $getPremioType =  Promotionalcode::select('premio_type')->where('code', $codigo)->first();


                        $mensaje = '4'.$getPremioType->premio_type;

                        /*if($mensaje == 41){
                            return view('sections.ganador-superman.main');
                        }else if($mensaje == 42){
                            return view('sections.ganador-flash.main');
                        }else if($mensaje == 43){
                            return view('sections.ganador-wonderwoman.main');
                        }*/
                        return $mensaje;
                    }
                }else{
                    //$mensaje = 'el código ya ha sido usado';
                    $mensaje = '1';
                    return $mensaje;
                }
            }else{

                //return 'no existe el code';
                $mensaje = '2';
                return $mensaje;
            }
        }else{
            //return 'no existe el code';
            $mensaje = '3';
            return $mensaje;
        }
    }

    public function winnerCodeSuperman(Request $request){
        return view('sections.ganador-superman.main');
    }

    public function winnerCodeWonderwoman(Request $request){
        return view('sections.ganador-wonderwoman.main');
    }

    public function winnerCodeFlash(Request $request){
        return view('sections.ganador-flash.main');
    }

    public function enviarDatos(Request $request) {

        $data = [];
        $data = Userinfo::create([
            'name'       => $request->input('name'),
            'fullname'   => $request->input('fullname'),
            'address'    => $request->input('address'),
            'cp'         => $request->input('cp'),
            'province'   => $request->input('province'),
            'local'      => $request->input('local'),
            'phone'      => $request->input('phone'),
            'bd'         => $request->input('bd')
        ]);

        return view('sections.confirmacion-datos.main');
    }
}
