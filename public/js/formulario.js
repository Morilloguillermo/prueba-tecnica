function enviar_catalogo(){
  var filter= /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
  var s_codigo_promocional = $('#promotionalCode').val();
  var s_email = $('#email').val();
  var s_bases_legales = $('#bases_legales').val();  
  var s_terminos_condiciones = $('#terminos_condiciones').val(); 
  var s_alta_newsletter = $('#alta_newsletter').val(); 
  var sendData = "true";

   if (s_codigo_promocional.length < 8  ){
    toastr["error"]('El campo "Código promocional" debe tener 8 caracteres y has escrito '+s_codigo_promocional.length)
    toastr.options = {
      "closeButton": false,
      "debug": true,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }   
    var sendData = "false";
  }else if (s_codigo_promocional.length == 8  ){
    $.ajax({
      data:  new FormData($("#catalogo_form")[0]),
      url:   '/verificar-codigo/'+s_codigo_promocional, 
      type:  'post',
      processData: false,
      contentType: false,
      success:  function (response) {
        console.log(response);
        if(response == 1){
          //toastr["error"]("el código ya ha sido usado");
          document.querySelector('#usedCodeErrorAlert').click();
        }else if(response == 2 || response == 3){
          //toastr["error"]("Código inválido");
          $(document).ready(function(){
              var goToNewOkurl = "/intentalo-nuevamente";
              $(location).attr('href', goToNewOkurl); // Using this
          });
        }else if(response == 41){
          //toastr["success"]("Código correcto");
          $(document).ready(function(){
              var goToNewOkurl = "/ganador-superman/"+s_codigo_promocional;
              $(location).attr('href', goToNewOkurl); // Using this
          });
        }else if(response == 42){
          //toastr["success"]("Código correcto");
          $(document).ready(function(){
              var goToNewOkurl = "/ganador-flash/"+s_codigo_promocional;
              $(location).attr('href', goToNewOkurl); // Using this
          });
        }else if(response == 43){
          //toastr["success"]("Código correcto");
          $(document).ready(function(){
              var goToNewOkurl = "/ganador-wonderwoman/"+s_codigo_promocional;
              $(location).attr('href', goToNewOkurl); // Using this
          });
        }
        
        toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
        $('form')[0].reset();
    }
    });
  }else{
    toastr["error"]('Campo "s_codigo_promocional" requerido')
    toastr.options = {
      "closeButton": false,
      "debug": true,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }   
    var sendData = "false";
  }


  if (filter.test(s_email)){ 
  }else{
    toastr["error"]("Correo electrónico inválido")
    toastr.options = {
      "closeButton": false,
      "debug": true,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }   
    sendData = "false";
  }

  if ($('#bases_legales').is(":checked")){
  }else{
    toastr["error"]('Campo "s_bases_legales" requerido')
    toastr.options = {
      "closeButton": false,
      "debug": true,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }   
    var sendData = "false";
  }
  
  if ($('#terminos_condiciones').is(":checked")){
  }else{
    toastr["error"]('Campo "s_terminos_condiciones" requerido');
    toastr.options = {
      "closeButton": false,
      "debug": true,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }   
    var sendData = "false";
  }  

 /* if ($('#alta_newsletter').is(":checked")){  
  }else{
    toastr["error"]('Campo "s_alta_newsletter" requerido')
    toastr.options = {
      "closeButton": false,
      "debug": true,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }   
    var sendData = "false";
  }  */ 
        
  if( sendData == "true" ) {
    var datos = {
      "codigo_promocional" : $('#promotionalCode').val(),
      "email" : $('#email').val(),
      "bases_legales" : $('#bases_legales').val(), 
      "terminos_condiciones" : $('#terminos_condiciones').val(), 
      "alta_newsletter" : $('#alta_newsletter').val(), 

      "cenviar" : $('#boton-enviar').val(),
      "_token": getCsrfToken()
    };

    $("#boton-enviar").text("Enviando...");

    $.ajax({
      data:  new FormData($("#catalogo_form")[0]),
      url:   '/enviar-catalogo', 
      type:  'post',
      processData: false,
      contentType: false,
      success:  function (response) {
        toastr["success"]("Solicitud enviada correctamente")
        toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
        $('form')[0].reset();
      }
    }).done(function() {}).fail(function() {
      //toastr["error"]("Ha ocurrido un error al enviar su solicitud")
        toastr.options = {
          "closeButton": false,
          "debug": true,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    }).always(function() {
      $("#boton-enviar").text("Comprobar");
    });

    grecaptcha.reset();
    return true;

  }else{
    grecaptcha.reset();
    return false;
  }
}

function getCsrfToken() { 
  var metas = document.getElementsByTagName('meta'); 
  for (var i=0; i<metas.length; i++) { 
    if (metas[i].getAttribute("name") == "csrf-token") { 
      return metas[i].getAttribute("content"); 
    } 
  } 
  return "";
}     







   






