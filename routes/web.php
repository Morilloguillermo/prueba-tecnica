<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('sections.home.main');
});

Route::get('/inicio', function () {
    return view('sections.inicio.main');
});

Route::get('/codigo', function () {
    return view('sections.codigo.main');
});

Route::get('/intentalo-nuevamente', function () {
    return view('sections.perdedor.main');
});

/*Route::get('/ganador-superman/{codigopromocional}', function () {
    return view('sections.ganador-superman.main');
});*/

Route::get('/ganador-superman/{codigopromocional}', 'ValidatecodeController@winnerCodeSuperman');

Route::get('/ganador-flash/{codigopromocional}', 'ValidatecodeController@winnerCodeFlash');

Route::get('/ganador-wonderwoman/{codigopromocional}', 'ValidatecodeController@winnerCodeWonderwoman');


Route::post('enviar-catalogo', 'ValidatecodeController@enviarCatalogo');
Route::post('enviar-datos', 'ValidatecodeController@enviarDatos')->name('enviarDatos');

Route::post('verificar-codigo/{codigo_promocional}', 'ValidatecodeController@verificarCodigo');

